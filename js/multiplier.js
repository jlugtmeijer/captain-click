

function buyFive() {
    if (amount ==5) {
        return
    } else if (amount == 10) {
        shovelPrice /= 10
        chestPrice /= 10
        amount /= 10

        shovelPrice *= 5
        chestPrice *= 5
        amount *= 5

        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    } else {
        shovelPrice *= 5
        chestPrice *= 5
        amount *= 5
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    }
}

function buyOne() {
    if (amount == 5) {
        shovelPrice /= 5
        chestPrice /= 5
        amount /= 5
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    } else if (amount > 9) {
        shovelPrice /= 10
        chestPrice /= 10
        amount /= 10
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    } else {
        return
    }
}

function buyTen() {
    if (amount == 5) {
        shovelPrice /= 5
        chestPrice /= 5
        amount /= 5

        shovelPrice *= 10
        chestPrice *= 10
        amount *= 10

        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    } else if (amount == 10) {
        return
    } else {
        shovelPrice *= 10
        chestPrice *= 10
        amount *= 10
        document.getElementById("shovelprice").innerHTML = shovelPrice
        document.getElementById("chestprice").innerHTML = chestPrice
    }
}

